package com.manage.dao;

import com.manage.base.IBaseDao;
import com.manage.domain.User;

public interface IUserDao extends IBaseDao<User> {

	public User login(User model);

}
