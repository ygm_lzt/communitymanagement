package com.manage.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.manage.base.BaseDaoImpl;
import com.manage.dao.IUserDao;
import com.manage.domain.User;
import com.manage.utils.MD5Utils;
/**
 * 用户管理
 *
 */
@Repository
public class UserDaoImpl extends BaseDaoImpl<User> implements IUserDao {

	/**
	 * 用户登录
	 */
	public User login(User user) {
		String hql = "FROM User u WHERE u.name = ? AND u.password = ?";
		Query query = this.getSession().createQuery(hql);
		query.setParameter(0, user.getName());
		query.setParameter(1, MD5Utils.md5(user.getPassword()));
		List<User> list = query.list();
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

}
