package com.manage.action;

import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.manage.base.BaseAction;
import com.manage.domain.User;
import com.manage.utils.MD5Utils;

/**
 * 用户管理
 */
@Controller
@Scope("prototype")
public class UserAction extends BaseAction<User>{
	/**
	 * 初始化密码
	 */
	public String initPassword(){
		//先查询
		User user = userService.getById(model.getId());
		user.setPassword(MD5Utils.md5("123456"));//设置密码为1234
		
		userService.update(user);
		return "toList";
	}
	
	/**
	 * 用户登录
	 */
	public String login(){
		System.out.println(model.getName());
		System.out.println(model.getPassword());
		User user = userService.login(model);
		
		if(user != null){
			//登录成功
			//将登录用户放入Session
			ServletActionContext.getRequest().getSession().setAttribute("loginUser", user);
			return "home";
		}else{
			//登录失败
			//设置错误提示
			this.addActionError("用户名或者密码错误！");
			return "loginUI";
		}
	}
	
	/**
	 * 用户退出系统
	 */
	public String logout(){
		//从Session中清除登录用户
		ServletActionContext.getRequest().getSession().removeAttribute("loginUser");
		
		//跳转到登录页面
		return "loginUI";
	}
}
