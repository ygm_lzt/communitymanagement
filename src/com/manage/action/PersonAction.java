package com.manage.action;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.manage.base.BaseAction;
import com.manage.domain.Person;

/**
 * 人员管理
 */
@Controller
@Scope("prototype")
public class PersonAction extends BaseAction<Person>{
	public String newPerson(){
		
		return "new";
	}
	
	public String editPerson(){
		
		return "edit";
	}
	
	public String listPerson(){
		
		return "list";
	}
	
	
}
