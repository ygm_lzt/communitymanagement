package com.manage.action;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.manage.base.BaseAction;
import com.manage.domain.User;

/**
 * 用户管理
 */
@Controller
@Scope("prototype")
public class SearchAction extends BaseAction<User>{
	
	public String search(){
		
		return SUCCESS;
	}
}
