package com.manage.domain;

import java.util.Date;

public class Person {
	private int id;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 性别1：男；2：女
	 */
	private int sex;
	/**
	 * 身份证号
	 */
	private String idcard;
	/**
	 * 民族
	 */
	private String nation;
	/**
	 * 联系电话
	 */
	private String phone;
	/**
	 * qq号
	 */
	private String qq;
	/**
	 * 电子邮件
	 */
	private String email;
	/**
	 * 工作单位
	 */
	private String workUnit;
	/**
	 * 现住址
	 */
	private String newAddress;
	/**
	 * 是否在住（1：是；2：否）
	 */
	private int isLive;
	
	/**
	 * 政治面貌(1；中共党员；2：群众；3：民主党派；4：团员)
	 */
	private int politicalStatus;
	
	/**
	 * 文化程度（1：小学；2：初中；3：高中；4：大学；5：中专；6：大专；7：研究生）
	 */
	private int degree;
	
	/**
	 * 身体状况（1：健康；2：肢体残疾；3：精神残疾）
	 */
	private int health;
	
	/**
	 * 空巢孤寡(1：是；2：否)
	 */
	private int houseAlone;
	
	/**
	 * 高龄人员(1：是；2：否)
	 */
	private int isHighage;
	
	/**
	 * 楼门院长(1：是；2：否)
	 */
	private int houseDirector;
	
	/**
	 * 文明养犬(1：无养；2：无证；3：有证)
	 */
	private int raiseDog;
	
	/**
	 * 关注类别(1：A；2：B；3：C)
	 */
	private int followType;
	
	/**
	 *  属于流动人员(1:是；null：不是)
	 */
	private int isFlowPerson;
	
	/**
	 * 户籍地址
	 */
	private String householdRegister;
	/**
	 * 户籍类别(1：农业；2：非农业)
	 */
	private int isAgriculture;
	
	/**
	 * 工作单位地址
	 */
	private String workUnitAddress;
	
	/**
	 * 单位行业(1：国企央企；2：事业单位；3：外企；4：私企；5：七小门店)
	 */
	private int unitTrade;
	
	/**
	 * 租金额
	 */
	private String rent;
	/**
	 * 租住人员类型(1：本市；2：外省市；3：港澳台；4：外籍)
	 */
	private int rentPersonType;
	
	/**
	 * 出租起始日期
	 */
	private Date rentStartTime;
	
	/**
	 * 出租截止日期
	 */
	private Date rentEndTime;
	
	/**
	 * 来京时间
	 */
	private Date comeBjTime;
	
	/**
	 * 来京目的(1：务工；2：探亲；3：旅游；4：外加京)
	 */
	private int comeBjPurpose;
	
	/**
	 * 居住年限
	 */
	private String 居住年限;
	
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 相片URL
	 */
	private String photo;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public String getIdcard() {
		return idcard;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	public String getNation() {
		return nation;
	}
	public void setNation(String nation) {
		this.nation = nation;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getWorkUnit() {
		return workUnit;
	}
	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}
	public String getNewAddress() {
		return newAddress;
	}
	public void setNewAddress(String newAddress) {
		this.newAddress = newAddress;
	}
	public int getIsLive() {
		return isLive;
	}
	public void setIsLive(int isLive) {
		this.isLive = isLive;
	}
	public int getPoliticalStatus() {
		return politicalStatus;
	}
	public void setPoliticalStatus(int politicalStatus) {
		this.politicalStatus = politicalStatus;
	}
	public int getDegree() {
		return degree;
	}
	public void setDegree(int degree) {
		this.degree = degree;
	}
	public int getHealth() {
		return health;
	}
	public void setHealth(int health) {
		this.health = health;
	}
	public int getHouseAlone() {
		return houseAlone;
	}
	public void setHouseAlone(int houseAlone) {
		this.houseAlone = houseAlone;
	}
	public int getIsHighage() {
		return isHighage;
	}
	public void setIsHighage(int isHighage) {
		this.isHighage = isHighage;
	}
	public int getHouseDirector() {
		return houseDirector;
	}
	public void setHouseDirector(int houseDirector) {
		this.houseDirector = houseDirector;
	}
	public int getRaiseDog() {
		return raiseDog;
	}
	public void setRaiseDog(int raiseDog) {
		this.raiseDog = raiseDog;
	}
	public int getFollowType() {
		return followType;
	}
	public void setFollowType(int followType) {
		this.followType = followType;
	}
	public int getIsFlowPerson() {
		return isFlowPerson;
	}
	public void setIsFlowPerson(int isFlowPerson) {
		this.isFlowPerson = isFlowPerson;
	}
	public String getHouseholdRegister() {
		return householdRegister;
	}
	public void setHouseholdRegister(String householdRegister) {
		this.householdRegister = householdRegister;
	}
	public int getIsAgriculture() {
		return isAgriculture;
	}
	public void setIsAgriculture(int isAgriculture) {
		this.isAgriculture = isAgriculture;
	}
	public String getWorkUnitAddress() {
		return workUnitAddress;
	}
	public void setWorkUnitAddress(String workUnitAddress) {
		this.workUnitAddress = workUnitAddress;
	}
	public int getUnitTrade() {
		return unitTrade;
	}
	public void setUnitTrade(int unitTrade) {
		this.unitTrade = unitTrade;
	}
	public String getRent() {
		return rent;
	}
	public void setRent(String rent) {
		this.rent = rent;
	}
	public int getRentPersonType() {
		return rentPersonType;
	}
	public void setRentPersonType(int rentPersonType) {
		this.rentPersonType = rentPersonType;
	}
	public Date getRentStartTime() {
		return rentStartTime;
	}
	public void setRentStartTime(Date rentStartTime) {
		this.rentStartTime = rentStartTime;
	}
	public Date getRentEndTime() {
		return rentEndTime;
	}
	public void setRentEndTime(Date rentEndTime) {
		this.rentEndTime = rentEndTime;
	}
	public Date getComeBjTime() {
		return comeBjTime;
	}
	public void setComeBjTime(Date comeBjTime) {
		this.comeBjTime = comeBjTime;
	}
	public int getComeBjPurpose() {
		return comeBjPurpose;
	}
	public void setComeBjPurpose(int comeBjPurpose) {
		this.comeBjPurpose = comeBjPurpose;
	}
	public String get居住年限() {
		return 居住年限;
	}
	public void set居住年限(String 居住年限) {
		this.居住年限 = 居住年限;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	
}
