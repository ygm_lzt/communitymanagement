package com.manage.service;

import com.manage.domain.User;

public interface IUserService {
	public User getById(Long id);
	
	public void update(User user);

	public User login(User model);
}
