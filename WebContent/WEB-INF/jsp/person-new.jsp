<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--[if IE 8]> <html lang="zh-cn" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="zh-cn" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-cn">
<!--<![endif]-->
<head>
<title>管理员 | 新建人员</title>

<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicon -->
<link rel="shortcut icon" href="favicon.ico">

<!-- Web Fonts -->

<!-- CSS Global Compulsory -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css">

<!-- CSS Header and Footer -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/headers/header-v7.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/footers/footer-v1.css">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/animate.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/line-icons/line-icons.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">

<!-- CSS Theme -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/theme-colors/blue.css" />

<!-- CSS Customization -->
</head>

<body>
<div class="wrapper admin">
<!--=== Header v7 Left ===-->
<div class="header-v7 header-left-v7">
  <nav class="navbar navbar-default mCustomScrollbar" role="navigation" data-mcs-theme="minimal-dark"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="menu-container"> 
      <!-- Toggle get grouped for better mobile display -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <!-- End Toggle --> 
      
      <!-- Logo -->
      <div class="logo middle color-white hidden-xs"> 大栅栏派出所铁树斜街社区</div>
      <!-- End Logo --> 
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-responsive-collapse">
      <div class="menu-container">
        <ul class="nav navbar-nav">
          <li class=" open active"> <a href="${pageContext.request.contextPath}/search.do"> <i class="fa fa-search margin-right-10"></i>综合搜索 </a> </li>
          <li class="dropdown"> <a class="dropdown-toggle"> <i class="fa fa-home margin-right-10"></i>房屋信息 </a>
            <ul class="dropdown-menu">
              <li class=""><a href="${pageContext.request.contextPath}/house_newHouse.do">新建房屋</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/house_listHouse.do">房屋列表</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a class="dropdown-toggle"> <i class="fa fa-users margin-right-10"></i>人员 </a>
            <ul class="dropdown-menu">
              <li class=""><a href="${pageContext.request.contextPath}/person_newPerson.do">新建人员</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/person_listPerson.do">人员列表</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a class="dropdown-toggle"> <i class="fa fa-tasks margin-right-10"></i>记事 </a>
            <ul class="dropdown-menu">
              <li class=""><a href="${pageContext.request.contextPath}/event_newEvent.do">新增记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_criminalList.do">刑事记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_securityList.do">治安记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_civilList.do">民事记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_goodList.do">好人好事记录</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
    <!-- End Navbar Collapse --> 
  </nav>
</div>
<!--=== End Header v7 Left ===--> 

<!--=== Content Side Left Right ===-->
<div class="content-side-right"> 
  <!-- Topbar -->
  <div class="admin-topbar">
    <div class="topbar-v1">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <ul style="margin-bottom:0px; " class="list-inline top-v1-data">
              <li><a href="login.html">登出</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Topbar --> 
  <!-- Content Boxes -->
  <div class="container content-sm manager">
    <div class="row">
      <div class="col-sm-12">
        <ul class="breadcrumb">
          <li><a>管理员</a></li>
          <li>人员</li>
          <li class="active">新建人员</li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="headline">
          <h4>新建人员</h4>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-12">
            <form action="#" id="sky-form4" class="sky-form">
              <fieldset style="padding:0px;">
                <section>
                  <label class="label">姓名：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">性别：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">男</option>
                      <option value="2">女</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">身份证号：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">民族：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">联系电话：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">QQ号码：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
              </fieldset>
            </form>
          </div>
          <div class="col-md-4 col-sm-12">
            <form action="#" id="sky-form4" class="sky-form">
              <fieldset style="padding:0px;">
                <section>
                  <label class="label">电子邮箱：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">工作单位：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">现住址：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">是否在住：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">是</option>
                      <option value="2">否</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">政治面貌：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">中共党员</option>
                      <option value="2">群众</option>
                      <option value="3">民主党派</option>
                      <option value="4">团员</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">文化程度：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">小学</option>
                      <option value="2">初中</option>
                      <option value="3">高中</option>
                      <option value="4">大学</option>
                      <option value="5">中专</option>
                      <option value="6">大专</option>
                      <option value="7">研究生</option>
                    </select>
                    <i></i> </label>
                </section>
              </fieldset>
            </form>
          </div>
          <div class="col-md-4 col-sm-12">
            <form action="#" id="sky-form4" class="sky-form">
              <fieldset style="padding:0px;">
                <section>
                  <label class="label">身体状况：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">健康</option>
                      <option value="2">肢体残疾</option>
                      <option value="3">精神残疾</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">空巢孤寡：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">是</option>
                      <option value="2">否</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">高龄人员：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">是</option>
                      <option value="2">否</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">楼门院长：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">是</option>
                      <option value="2">否</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">文明养犬：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">无养</option>
                      <option value="2">无证</option>
                      <option value="3">有证</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">关注类别：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">A</option>
                      <option value="2">B</option>
                      <option value="3">C</option>
                    </select>
                    <i></i> </label>
                </section>
              </fieldset>
            </form>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <form action="#" id="sky-form4" class="sky-form">
              <fieldset style="padding:0px;">
                <hr class="hr-sm"/>
                <section>
                  <label class="checkbox">
                    <input name="checkbox" type="checkbox">
                    <i></i>属于流动人员 </label>
                </section>
              </fieldset>
            </form>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-12"> 
            <!-- Reg-Form -->
            <form action="#" id="sky-form4" class="sky-form">
              <fieldset style="padding:0px;">
                <section>
                  <label class="label">户籍地址：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">户籍类别：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">农业</option>
                      <option value="2">非农业</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">工作单位地址：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">单位行业：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">国企央企</option>
                      <option value="2">事业单位</option>
                      <option value="3">外企</option>
                      <option value="4">私企</option>
                      <option value="5">七小门店</option>
                    </select>
                    <i></i> </label>
                </section>
              </fieldset>
            </form>
          </div>
          <div class="col-md-4 col-sm-12"> 
            <!-- Reg-Form -->
            <form action="#" id="sky-form4" class="sky-form">
              <fieldset style="padding:0px;">
                <section>
                  <label class="label">租金额：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">租住人员类型：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">本市</option>
                      <option value="2">外省市</option>
                      <option value="3">港澳台</option>
                      <option value="4">外籍</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">出租起始日期：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">出租截止日期：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
              </fieldset>
            </form>
          </div>
          <div class="col-md-4 col-sm-12">
            <form action="#" id="sky-form4" class="sky-form">
              <fieldset style="padding:0px;">
                <section>
                  <label class="label">来京时间：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">来京目的：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">务工</option>
                      <option value="2">探亲</option>
                      <option value="3">旅游</option>
                      <option value="4">外加京</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">居住年限：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">相片：</label>
                  <label class="select">
                  <button type="submit" class="btn-u">上传</button>
                  <label class="margin-left-10">xxx.jpg </label>
                  </label>
                </section>
              </fieldset>
            </form>
          </div>
          <div class="col-sm-12">
            <form action="#" id="sky-form4" class="sky-form">
              <fieldset style="padding:0px;">
                <hr class="hr-sm">
                <section>
                  <label class="label">备注：</label>
                  <textarea class="form-control" rows="6"></textarea>
                </section>
                <section class=" color-red">
                  <label class="label"><b>错误提示信息</b></label>
                </section>
              </fieldset>
              <footer style="padding:0px;">
                <button type="submit" class="btn-u">保存</button>
              </footer>
            </form>
          </div>
        </div>
      </div>
    </div>
    
    <!--/end container--> 
    <!-- End Content Boxes --> 
    
  </div>
  <!--=== End Content Side Left Right ===--> 
</div>
<!--/wrapper--> 

<!-- JS Global Compulsory --> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery/jquery-migrate.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins --> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/back-to-top.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery-appear.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/smoothScroll.js"></script> 
<!-- JS Customization --> 
<!-- JS Page Level --> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/app.js"></script> 
<script type="text/javascript">
    jQuery(document).ready(function() {
      	App.init();
        App.initSidebarMenuDropdown();
       
    });
</script> 
<!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/plugins/respond.js"></script>
    <script src="${pageContext.request.contextPath}/assets/plugins/html5shiv.js"></script>
    <script src="${pageContext.request.contextPath}/assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>