<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--[if IE 8]> <html lang="zh-cn" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="zh-cn" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-cn">
<!--<![endif]-->
<head>
<title>管理员 | 修改记录</title>

<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicon -->
<link rel="shortcut icon" href="favicon.ico">

<!-- Web Fonts -->

<!-- CSS Global Compulsory -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css">

<!-- CSS Header and Footer -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/headers/header-v7.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/footers/footer-v1.css">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/animate.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/line-icons/line-icons.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">

<!-- CSS Theme -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/theme-colors/blue.css" />

<!-- CSS Customization -->
</head>

<body>
<div class="wrapper admin">
<!--=== Header v7 Left ===-->
<div class="header-v7 header-left-v7">
  <nav class="navbar navbar-default mCustomScrollbar" role="navigation" data-mcs-theme="minimal-dark"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="menu-container"> 
      <!-- Toggle get grouped for better mobile display -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <!-- End Toggle --> 
      
      <!-- Logo -->
      <div class="logo middle color-white hidden-xs"> 大栅栏派出所铁树斜街社区</div>
      <!-- End Logo --> 
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-responsive-collapse">
      <div class="menu-container">
         <ul class="nav navbar-nav">
          <li class=" open active"> <a href="${pageContext.request.contextPath}/search.do"> <i class="fa fa-search margin-right-10"></i>综合搜索 </a> </li>
          <li class="dropdown"> <a class="dropdown-toggle"> <i class="fa fa-home margin-right-10"></i>房屋信息 </a>
            <ul class="dropdown-menu">
              <li class=""><a href="${pageContext.request.contextPath}/house_newHouse.do">新建房屋</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/house_listHouse.do">房屋列表</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a class="dropdown-toggle"> <i class="fa fa-users margin-right-10"></i>人员 </a>
            <ul class="dropdown-menu">
              <li class=""><a href="${pageContext.request.contextPath}/person_newPerson.do">新建人员</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/person_listPerson.do">人员列表</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a class="dropdown-toggle"> <i class="fa fa-tasks margin-right-10"></i>记事 </a>
            <ul class="dropdown-menu">
              <li class=""><a href="${pageContext.request.contextPath}/event_newEvent.do">新增记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_criminalList.do">刑事记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_securityList.do">治安记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_civilList.do">民事记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_goodList.do">好人好事记录</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
    <!-- End Navbar Collapse --> 
  </nav>
</div>
<!--=== End Header v7 Left ===--> 

<!--=== Content Side Left Right ===-->
<div class="content-side-right"> 
  <!-- Topbar -->
  <div class="admin-topbar">
    <div class="topbar-v1">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <ul style="margin-bottom:0px; " class="list-inline top-v1-data">
              <li><a href="login.html">登出</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Topbar --> 
  <!-- Content Boxes -->
  <div class="container content-sm manager">
    <div class="row">
      <div class="col-sm-12">
        <ul class="breadcrumb">
          <li><a>管理员</a></li>
          <li>记录</li>
          <li class="active">修改记录</li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div id="new" class="col-sm-12">
        <div class="headline">
          <h4>修改记录</h4>
        </div>
        <div class="row">
          <div class="col-sm-12"> 
            <!-- Reg-Form -->
            <form action="#" id="sky-form4" class="sky-form">
              <fieldset style="padding:0px;">
                <section>
                  <label class="label">记录类型：</label>
                  <div class="row">
                    <div class="col col-3">
                      <label class="radio">
                        <input type="radio" name="radio" checked="">
                        <i class="rounded-x"></i>刑事记录</label>
                    </div>
                    <div class="col col-3">
                      <label class="radio">
                        <input type="radio" name="radio">
                        <i class="rounded-x"></i>治安记录</label>
                    </div>
                    <div class="col col-3">
                      <label class="radio">
                        <input type="radio" name="radio">
                        <i class="rounded-x"></i>民事记录</label>
                    </div>
                    <div class="col col-3">
                      <label class="radio">
                        <input type="radio" name="radio">
                        <i class="rounded-x"></i>好人好事记录</label>
                    </div>
                  </div>
                </section>
                <hr class="hr-sm"/>
                <section>
                  <label class="label">涉事相关人员：（下面的选择框，做成输入文字过滤相关姓名，并点击选择。）</label>
                  <div class="row">
                    <div class="col col-6">
                      <label class="select">
                        <select name="invoice-content">
                          <option value="0" selected disabled>选择人员</option>
                          <option value="1">张三三（证件号：2115781988010245687）</option>
                          <option value="2">张天天（证件号：2116781988010245687）</option>
                        </select>
                        <i></i> </label>
                    </div>
                    <div class="col col-4">
                      <button type="submit" class="btn-u">添加</button>
                    </div>
                  </div>
                  <br/>
                  <div class="row admin-box1">
                    <div class="col col-6">
                      <div class="panel panel-blue margin-bottom-10 table-responsive">
                        <div class="panel-heading">
                          <h3 class="panel-title"> 已添加人员</h3>
                        </div>
                        <table class="table table-hover sky-form">
                          <thead>
                            <tr>
                              <th>名字</th>
                              <th>身份证号</th>
                              <th>查看</th>
                              <th>删除</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>张三</td>
                              <td>1234561982120112345</td>
                              <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                              <td><button class="btn btn-danger btn-xs">删除</button></td>
                            </tr>
                            <tr>
                              <td>张三</td>
                              <td>1234561982120112345</td>
                              <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                              <td><button class="btn btn-danger btn-xs">删除</button></td>
                            </tr>
                            <tr>
                              <td>张三</td>
                              <td>1234561982120112345</td>
                              <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                              <td><button class="btn btn-danger btn-xs">删除</button></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </section>
                <hr class="hr-sm"/>
                <section>
                  <label class="label">涉事相关房屋：（下面的选择框，做成输入文字过滤相关房屋，并点击选择。）</label>
                  <div class="row">
                    <div class="col col-6">
                      <label class="select">
                        <select name="invoice-content">
                          <option value="0" selected disabled>选择房屋</option>
                          <option value="1">房屋地址1房屋地址1房屋地址1房屋地址1房屋地址1</option>
                          <option value="2">房屋地址1房屋地址1房屋地址1房屋地址1房屋地址2</option>
                        </select>
                        <i></i> </label>
                    </div>
                    <div class="col col-4">
                      <button type="submit" class="btn-u">添加</button>
                    </div>
                  </div>
                  <br/>
                  <div class="row admin-box1">
                    <div class="col col-6">
                      <div class="panel panel-blue margin-bottom-10 table-responsive">
                        <div class="panel-heading">
                          <h3 class="panel-title"> 已添加房屋</h3>
                        </div>
                        <table class="table table-hover sky-form">
                          <thead>
                            <tr>
                              <th>地址</th>
                              <th>查看</th>
                              <th>删除</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>房屋地址1房屋地址1房屋地址1房屋地址1房屋地址1</td>
                              <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                              <td><button class="btn btn-danger btn-xs">删除</button></td>
                            </tr>
                            <tr>
                              <td>房屋地址1房屋地址1房屋地址1房屋地址1房屋地址123</td>
                              <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                              <td><button class="btn btn-danger btn-xs">删除</button></td>
                            </tr>
                            <tr>
                              <td>房屋地址1房屋地址1房屋地址1房屋地址1房屋地址145</td>
                              <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                              <td><button class="btn btn-danger btn-xs">删除</button></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </section>
                <hr class="hr-sm"/>
                <section>
                  <label class="label">事件发生日期：（时间选择框。默认给出记录当天时间）</label>
                  <div class="row">
                    <div class="col col-6">
                      <label class="input">
                        <input type="text" name="address" placeholder="选择时间">
                      </label>
                    </div>
                  </div>
                </section>
                <hr class="hr-sm"/>
                <section>
                  <label class="label">记录人员：</label>
                  <div class="row">
                    <div class="col col-6">
                      <label class="input">
                        <input type="text" name="address" placeholder="张思">
                      </label>
                    </div>
                  </div>
                </section>
                <hr class="hr-sm"/>
                <section>
                  <label class="label">处理人员：</label>
                  <div class="row">
                    <div class="col col-6">
                      <label class="input">
                        <input type="text" name="address" placeholder="王武">
                      </label>
                    </div>
                  </div>
                </section>
                <hr class="hr-sm"/>
                <section>
                  <label class="label">输入事件详情：</label>
                  <textarea class="form-control" rows="15">事项详细记述事项详细记述事项详细记述事项详细记述事项详细记述事项详细记述</textarea>
                </section>
              </fieldset>
              <footer style="padding:0px;">
                <button type="submit" class="btn-u">保存</button>
              </footer>
            </form>
          </div>
        </div>
      </div>
    </div>
    
    <!--/end container--> 
    <!-- End Content Boxes --> 
    
  </div>
  <!--=== End Content Side Left Right ===--> 
</div>
<!--/wrapper--> 

<!-- JS Global Compulsory --> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery/jquery-migrate.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins --> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/back-to-top.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery-appear.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/smoothScroll.js"></script> 
<!-- JS Customization --> 
<!-- JS Page Level --> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/app.js"></script> 
<script type="text/javascript">
    jQuery(document).ready(function() {
      	App.init();
        App.initSidebarMenuDropdown();
       
    });
</script> 
<!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/plugins/respond.js"></script>
    <script src="${pageContext.request.contextPath}/assets/plugins/html5shiv.js"></script>
    <script src="${pageContext.request.contextPath}/assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>