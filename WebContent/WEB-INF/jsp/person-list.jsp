<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--[if IE 8]> <html lang="zh-cn" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="zh-cn" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-cn">
<!--<![endif]-->
<head>
<title>管理员 | 人员</title>

<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicon -->
<link rel="shortcut icon" href="favicon.ico">

<!-- Web Fonts -->

<!-- CSS Global Compulsory -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css">

<!-- CSS Header and Footer -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/headers/header-v7.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/footers/footer-v1.css">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/animate.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/line-icons/line-icons.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">

<!-- CSS Theme -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/theme-colors/blue.css" />

<!-- CSS Customization -->
</head>

<body>
<div class="wrapper admin">
<!--=== Header v7 Left ===-->
<div class="header-v7 header-left-v7">
  <nav class="navbar navbar-default mCustomScrollbar" role="navigation" data-mcs-theme="minimal-dark"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="menu-container"> 
      <!-- Toggle get grouped for better mobile display -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <!-- End Toggle --> 
      
      <!-- Logo -->
      <div class="logo middle color-white hidden-xs"> 大栅栏派出所铁树斜街社区</div>
      <!-- End Logo --> 
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-responsive-collapse">
      <div class="menu-container">
        <ul class="nav navbar-nav">
          <li class=" open active"> <a href="${pageContext.request.contextPath}/search.do"> <i class="fa fa-search margin-right-10"></i>综合搜索 </a> </li>
          <li class="dropdown"> <a class="dropdown-toggle"> <i class="fa fa-home margin-right-10"></i>房屋信息 </a>
            <ul class="dropdown-menu">
              <li class=""><a href="${pageContext.request.contextPath}/house_newHouse.do">新建房屋</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/house_listHouse.do">房屋列表</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a class="dropdown-toggle"> <i class="fa fa-users margin-right-10"></i>人员 </a>
            <ul class="dropdown-menu">
              <li class=""><a href="${pageContext.request.contextPath}/person_newPerson.do">新建人员</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/person_listPerson.do">人员列表</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a class="dropdown-toggle"> <i class="fa fa-tasks margin-right-10"></i>记事 </a>
            <ul class="dropdown-menu">
              <li class=""><a href="${pageContext.request.contextPath}/event_newEvent.do">新增记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_criminalList.do">刑事记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_securityList.do">治安记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_civilList.do">民事记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_goodList.do">好人好事记录</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
    <!-- End Navbar Collapse --> 
  </nav>
</div>
<!--=== End Header v7 Left ===--> 

<!--=== Content Side Left Right ===-->
<div class="content-side-right"> 
  <!-- Topbar -->
  <div class="admin-topbar">
    <div class="topbar-v1">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <ul style="margin-bottom:0px; " class="list-inline top-v1-data">
              <li><a href="login.html">登出</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Topbar --> 
  <!-- Content Boxes -->
  <div class="container content-sm manager">
    <div class="row">
      <div class="col-sm-12">
        <ul class="breadcrumb">
          <li><a>管理员</a></li>
          <li>人员</li>
          <li class="active">人员列表</li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12"> 
        <!-- Accordion v1 -->
        <div class="panel-group acc-v2" id="accordion-2">
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-title ">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="order-title" >
                      <div class="row little-btn-u">
                        <div class="col-sm-12">
                          <ul class="list-inline sky-form ">
                            <li class="col-md-1 col-sm-2">姓名：张三</li>
                            <li class="col-md-1 col-sm-2">性别：男</li>
                            <li class="col-md-3 col-sm-8">身份证号：123214199911221236</li>
                            <li class="pull-right"> <a href="${pageContext.request.contextPath}/person_editPerson.do" class="btn-u btn-u-blue btn-xs">修改</a> </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <hr class="hr-zdy1"/>
                  </div>
                  <div class="col-xs-12 order-toggle" > <a class="accordion-toggle list-group-item list-toggle" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-1">
                    <p>联系电话：13112345678</p>
                    <p> 现住址：xxxx街道xxx区xxxxxxx </p>
                    </a> </div>
                </div>
              </div>
            </div>
            <div id="collapse-1" class="panel-collapse collapse">
              <div class="panel-body  table-responsive">
                <div class="row margin-bottom-10">
                  <div class="col-md-4 col-sm-12">
                    <table class="lists-v1 p-table">
                      <tbody>
                        <tr>
                          <td>是否在住：</td>
                          <td>是</td>
                        </tr>
                        <tr>
                          <td>民族：</td>
                          <td>汉族</td>
                        </tr>
                        <tr>
                          <td>QQ号码：</td>
                          <td>132456789</td>
                        </tr>
                        <tr>
                          <td>电子邮箱：</td>
                          <td>132456789@qq.com</td>
                        </tr>
                        <tr>
                          <td>政治面貌：</td>
                          <td>中共党员</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <table class="lists-v1 p-table">
                      <tbody>
                        <tr>
                          <td>文化程度：</td>
                          <td>小学</td>
                        </tr>
                        <tr>
                          <td>工作单位：</td>
                          <td>xxxxxxxxxxxxx</td>
                        </tr>
                        <tr>
                          <td>身体状况：</td>
                          <td>健康</td>
                        </tr>
                        <tr>
                          <td>空巢孤寡：</td>
                          <td>是</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <table class="lists-v1 p-table">
                      <tbody>
                        <tr>
                          <td>高龄人员：</td>
                          <td>是</td>
                        </tr>
                        <tr>
                          <td>楼门院长：</td>
                          <td>是</td>
                        </tr>
                        <tr>
                          <td>文明养犬：</td>
                          <td>无养</td>
                        </tr>
                        <tr>
                          <td>关注类别：</td>
                          <td>A</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <hr/>
                <p>备注：备注备注备注备注备注备注备注备注备注备注，备注备注备注备注备注。</p>
                <hr/>
                <div class="row admin-box1 margin-bottom-20">
                  <div class="col-md-12">
                    <div class="panel panel-blue margin-bottom-10 table-responsive">
                      <div class="panel-heading">
                        <h3 style="background: transparent; font-size: 16px; padding: 5px 15px; color:#fff;" class="panel-title">住房经历</h3>
                      </div>
                      <table class="table table-striped table-hover sky-form">
                        <thead>
                          <tr>
                            <th>房屋地址</th>
                            <th>身份状态</th>
                            <th>开始日期</th>
                            <th>结束日期</th>
                            <th>租金额（元）</th>
                            <th>备注</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>xxxx街道xxx区xxxxxxx</td>
                            <td>流动人员</td>
                            <td>2016-01-01</td>
                            <td>2016-01-01</td>
                            <td>500</td>
                            <td>租房</td>
                          </tr>
                          <tr>
                            <td>xxxx街道xxx区xxxxxxx</td>
                            <td>户主</td>
                            <td>2016-01-01</td>
                            <td>2016-01-01</td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>xxxx街道xxx区xxxxxxx</td>
                            <td>房主</td>
                            <td>2016-01-01</td>
                            <td>2016-01-01</td>
                            <td></td>
                            <td></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-title ">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="order-title" >
                      <div class="row little-btn-u">
                        <div class="col-sm-12">
                          <ul class="list-inline sky-form ">
                            <li class="col-md-1 col-sm-2">姓名：张三</li>
                            <li class="col-md-1 col-sm-2">性别：男</li>
                            <li class="col-md-3 col-sm-8">身份证号：123214199911221236</li>
                            <li class="pull-right"> <a href="${pageContext.request.contextPath}/person_editPerson.do" class="btn-u btn-u-blue btn-xs">修改</a> </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <hr class="hr-zdy1"/>
                  </div>
                  <div class="col-xs-12 order-toggle" > <a class="accordion-toggle list-group-item list-toggle" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-2">
                    <p>联系电话：13112345678</p>
                    <p> 现住址：xxxx街道xxx区xxxxxxx </p>
                    </a> </div>
                </div>
              </div>
            </div>
            <div id="collapse-2" class="panel-collapse collapse">
              <div class="panel-body  table-responsive">
                <div class="row margin-bottom-10">
                  <div class="col-md-4 col-sm-12">
                    <table class="lists-v1 p-table">
                      <tbody>
                        <tr>
                          <td>是否在住：</td>
                          <td>是</td>
                        </tr>
                        <tr>
                          <td>民族：</td>
                          <td>汉族</td>
                        </tr>
                        <tr>
                          <td>QQ号码：</td>
                          <td>132456789</td>
                        </tr>
                        <tr>
                          <td>电子邮箱：</td>
                          <td>132456789@qq.com</td>
                        </tr>
                        <tr>
                          <td>政治面貌：</td>
                          <td>中共党员</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <table class="lists-v1 p-table">
                      <tbody>
                        <tr>
                          <td>文化程度：</td>
                          <td>小学</td>
                        </tr>
                        <tr>
                          <td>工作单位：</td>
                          <td>xxxxxxxxxxxxx</td>
                        </tr>
                        <tr>
                          <td>身体状况：</td>
                          <td>健康</td>
                        </tr>
                        <tr>
                          <td>空巢孤寡：</td>
                          <td>是</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <table class="lists-v1 p-table">
                      <tbody>
                        <tr>
                          <td>高龄人员：</td>
                          <td>是</td>
                        </tr>
                        <tr>
                          <td>楼门院长：</td>
                          <td>是</td>
                        </tr>
                        <tr>
                          <td>文明养犬：</td>
                          <td>无养</td>
                        </tr>
                        <tr>
                          <td>关注类别：</td>
                          <td>A</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <hr/>
                <p>备注：备注备注备注备注备注备注备注备注备注备注，备注备注备注备注备注。</p>
                <hr/>
                <div class="row admin-box1 margin-bottom-20">
                  <div class="col-md-12">
                    <div class="panel panel-blue margin-bottom-10 table-responsive">
                      <div class="panel-heading">
                        <h3 style="background: transparent; font-size: 16px; padding: 5px 15px; color:#fff;" class="panel-title">住房经历</h3>
                      </div>
                      <table class="table table-striped table-hover sky-form">
                        <thead>
                          <tr>
                            <th>房屋地址</th>
                            <th>身份状态</th>
                            <th>开始日期</th>
                            <th>结束日期</th>
                            <th>租金额（元）</th>
                            <th>备注</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>xxxx街道xxx区xxxxxxx</td>
                            <td>流动人员</td>
                            <td>2016-01-01</td>
                            <td>2016-01-01</td>
                            <td>500</td>
                            <td>租房</td>
                          </tr>
                          <tr>
                            <td>xxxx街道xxx区xxxxxxx</td>
                            <td>户主</td>
                            <td>2016-01-01</td>
                            <td>2016-01-01</td>
                            <td></td>
                            <td></td>
                          </tr>
                          <tr>
                            <td>xxxx街道xxx区xxxxxxx</td>
                            <td>房主</td>
                            <td>2016-01-01</td>
                            <td>2016-01-01</td>
                            <td></td>
                            <td></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Accordion v1 --> 
        
      </div>
    </div>
    <!--/end container--> 
    <!-- End Content Boxes -->
    <div class="text-right">
      <ul class="pagination">
        <li><a href="#">«</a></li>
        <li><a href="#">1</a></li>
        <li class="active"><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">»</a></li>
      </ul>
    </div>
  </div>
  <!--=== End Content Side Left Right ===--> 
</div>
<!--/wrapper--> 

<!-- JS Global Compulsory --> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery/jquery-migrate.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins --> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/back-to-top.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery-appear.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/smoothScroll.js"></script> 
<!-- JS Customization --> 
<!-- JS Page Level --> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/app.js"></script> 
<script type="text/javascript">
    jQuery(document).ready(function() {
      	App.init();
        App.initSidebarMenuDropdown();
       
    });
</script> 
<!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/plugins/respond.js"></script>
    <script src="${pageContext.request.contextPath}/assets/plugins/html5shiv.js"></script>
    <script src="${pageContext.request.contextPath}/assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>