<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--[if IE 8]> <html lang="zh-cn" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="zh-cn" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="zh-cn">
<!--<![endif]-->
<head>
<title>管理员 | 房屋信息</title>

<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicon -->
<link rel="shortcut icon" href="favicon.ico">

<!-- Web Fonts -->

<!-- CSS Global Compulsory -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/style.css">

<!-- CSS Header and Footer -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/headers/header-v7.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/footers/footer-v1.css">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/animate.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/line-icons/line-icons.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">

<!-- CSS Theme -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/theme-colors/blue.css" />

<!-- CSS Customization -->
</head>

<body>
<div class="wrapper admin">
<!--=== Header v7 Left ===-->
<div class="header-v7 header-left-v7">
  <nav class="navbar navbar-default mCustomScrollbar" role="navigation" data-mcs-theme="minimal-dark"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="menu-container"> 
      <!-- Toggle get grouped for better mobile display -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <!-- End Toggle --> 
      
      <!-- Logo -->
      <div class="logo middle color-white hidden-xs"> 大栅栏派出所铁树斜街社区</div>
      <!-- End Logo --> 
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-responsive-collapse">
      <div class="menu-container">
         <ul class="nav navbar-nav">
          <li class=" open active"> <a href="${pageContext.request.contextPath}/search.do"> <i class="fa fa-search margin-right-10"></i>综合搜索 </a> </li>
          <li class="dropdown"> <a class="dropdown-toggle"> <i class="fa fa-home margin-right-10"></i>房屋信息 </a>
            <ul class="dropdown-menu">
              <li class=""><a href="${pageContext.request.contextPath}/house_newHouse.do">新建房屋</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/house_listHouse.do">房屋列表</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a class="dropdown-toggle"> <i class="fa fa-users margin-right-10"></i>人员 </a>
            <ul class="dropdown-menu">
              <li class=""><a href="${pageContext.request.contextPath}/person_newPerson.do">新建人员</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/person_listPerson.do">人员列表</a></li>
            </ul>
          </li>
          <li class="dropdown"> <a class="dropdown-toggle"> <i class="fa fa-tasks margin-right-10"></i>记事 </a>
            <ul class="dropdown-menu">
              <li class=""><a href="${pageContext.request.contextPath}/event_newEvent.do">新增记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_criminalList.do">刑事记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_securityList.do">治安记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_civilList.do">民事记录</a></li>
              <li class=""><a href="${pageContext.request.contextPath}/event_goodList.do">好人好事记录</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
    <!-- End Navbar Collapse --> 
  </nav>
</div>
<!--=== End Header v7 Left ===--> 

<!--=== Content Side Left Right ===-->
<div class="content-side-right"> 
  <!-- Topbar -->
  <div class="admin-topbar">
    <div class="topbar-v1">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <ul style="margin-bottom:0px; " class="list-inline top-v1-data">
              <li><a href="login.html">登出</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Topbar --> 
  <!-- Content Boxes -->
  <div class="container content-sm manager">
    <div class="row">
      <div class="col-sm-12">
        <ul class="breadcrumb">
          <li><a>管理员</a></li>
          <li>房屋信息</li>
          <li class="active">新建房屋</li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="headline">
          <h4>新建房屋</h4>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-12">
            <form action="#" id="sky-form4" class="sky-form">
              <fieldset style="padding:0px;">
                <section>
                  <label class="label">房屋地址：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">房屋性质：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">单位承租公房</option>
                      <option value="2">个人承租公房</option>
                      <option value="3">私房</option>
                      <option value="4">自建房</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">房屋结构：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">砖木</option>
                      <option value="2">木质</option>
                      <option value="3">砖混</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">房屋层数：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="3">4</option>
                    </select>
                    <i></i> </label>
                </section>
              </fieldset>
            </form>
          </div>
          <div class="col-md-4 col-sm-12">
            <form action="#" id="sky-form4" class="sky-form">
              <fieldset style="padding:0px;">
                <section>
                  <label class="label">房屋高度：</label>
                  <label class="input">
                    <input type="text" name="address" placeholder="输入">
                  </label>
                </section>
                <section>
                  <label class="label">房屋锁具：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">防盗门</option>
                      <option value="2">挂锁</option>
                      <option value="2">防撬锁</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">房屋使用状况：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">自住</option>
                      <option value="2">群租</option>
                      <option value="1">家庭租</option>
                      <option value="2">空房</option>
                      <option value="2">腾退</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">出租类型：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">个人直租</option>
                      <option value="2">中介出租</option>
                    </select>
                    <i></i> </label>
                </section>
              </fieldset>
            </form>
          </div>
          <div class="col-md-4 col-sm-12">
            <form action="#" id="sky-form4" class="sky-form">
              <fieldset style="padding:0px;">
                <section>
                  <label class="label">出租合同：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">有</option>
                      <option value="2">无</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">出租用途：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">居住</option>
                      <option value="2">员工宿舍</option>
                      <option value="3">加工生产销售</option>
                      <option value="3">加工生产销售居住一体</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">签订治安责任书：</label>
                  <label class="select">
                    <select name="invoice-content">
                      <option value="0" selected disabled>选择</option>
                      <option value="1">是</option>
                      <option value="2">否</option>
                    </select>
                    <i></i> </label>
                </section>
                <section>
                  <label class="label">房屋平面图：</label>
                  <label class="select">
                  <button type="submit" class="btn-u">上传</button>
                  <label class="margin-left-10">xxx.jpg </label>
                  </label>
                </section>
              </fieldset>
            </form>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <button type="submit" class="btn-u">保存</button>
          </div>
        </div>
        <hr class="hr-sm"/>
        <div class="row">
          <div class="col-md-12 col-sm-12 margin-bottom-20">
            <div class="headline">
              <h4>房主信息</h4>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <form action="#" id="sky-form4" class="sky-form">
                  <fieldset style="padding:0px;">
                    <div class="row">
                      <div  class="col-sm-9">
                        <sectionn>
                        <label class="select">
                          <select name="invoice-content">
                            <option value="0" selected disabled>选择房主</option>
                            <option value="1">张三三（证件号：2115781988010245687）</option>
                            <option value="2">张天天（证件号：2116781988010245687）</option>
                          </select>
                          <i></i> </label>
                        </section>
                      </div>
                      <div  class="col-sm-3">
                        <sectionn>
                        <label class="select">
                          <button type="submit" class="btn-u btn-block">添加</button>
                        </label>
                        </section>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
            <br/>
            <div class="row admin-box1">
              <div class="col-sm-12">
                <div class="panel panel-grey margin-bottom-10 table-responsive">
                  <div class="panel-heading">
                    <h3 class="panel-title"> 已添加房主</h3>
                  </div>
                  <table class="table table-hover sky-form">
                    <thead>
                      <tr>
                        <th>名字</th>
                        <th>身份证号</th>
                        <th>开始时间</th>
                        <th>结束时间</th>
                        <th>查看</th>
                        <th>删除</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>张三一</td>
                        <td>1234561982120112345</td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                        <td><button class="btn btn-danger btn-xs">删除</button></td>
                      </tr>
                      <tr>
                        <td>张四一</td>
                        <td>1234561982120112345</td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                        <td><button class="btn btn-danger btn-xs">删除</button></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 margin-bottom-20">
            <div class="headline">
              <h4>户主信息</h4>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <form action="#" id="sky-form4" class="sky-form">
                  <fieldset style="padding:0px;">
                    <div class="row">
                      <div  class="col-sm-9">
                        <sectionn>
                        <label class="input">
                          <input type="text" name="address" placeholder="房屋地址">
                        </label>
                        </section>
                      </div>
                    </div>
                    <div class="row">
                      <div  class="col-sm-9">
                        <sectionn>
                        <label class="select">
                          <select name="invoice-content">
                            <option value="0" selected disabled>选择户主</option>
                            <option value="1">张三三（证件号：2115781988010245687）</option>
                            <option value="2">张天天（证件号：2116781988010245687）</option>
                          </select>
                          <i></i> </label>
                        </section>
                      </div>
                      <div  class="col-sm-3">
                        <sectionn>
                        <label class="select">
                          <button type="submit" class="btn-u btn-block">添加</button>
                        </label>
                        </section>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
            <br/>
            <div class="row admin-box1">
              <div class="col-sm-12">
                <div class="panel panel-grey margin-bottom-10 table-responsive">
                  <div class="panel-heading">
                    <h3 class="panel-title"> 已添加户主</h3>
                  </div>
                  <table class="table table-hover sky-form">
                    <thead>
                      <tr>
                        <th>名字</th>
                        <th>身份证号</th>
                        <th>房屋地址</th>
                        <th>开始时间</th>
                        <th>结束时间</th>
                        <th>查看</th>
                        <th>删除</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>张三</td>
                        <td>1234561982120112345</td>
                        <td>xxxxxxxxx</td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                        <td><button class="btn btn-danger btn-xs">删除</button></td>
                      </tr>
                      <tr>
                        <td>张四</td>
                        <td>1234561982120112345</td>
                        <td>xxxxxxxxx</td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                        <td><button class="btn btn-danger btn-xs">删除</button></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row margin-bottom-20">
          <div class="col-md-12 col-sm-12">
            <div class="headline">
              <h4>户主：张三</h4>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <form action="#" id="sky-form4" class="sky-form">
                  <fieldset style="padding:0px;">
                    <div class="row">
                      <div  class="col-sm-9">
                        <sectionn>
                        <label class="select">
                          <select name="invoice-content">
                            <option value="0" selected disabled>选择其他用户</option>
                            <option value="1">张三三（证件号：2115781988010245687）</option>
                            <option value="2">张天天（证件号：2116781988010245687）</option>
                          </select>
                          <i></i> </label>
                        </section>
                      </div>
                      <div  class="col-sm-3">
                        <sectionn>
                        <label class="select">
                          <button type="submit" class="btn-u btn-block">添加</button>
                        </label>
                        </section>
                      </div>
                    </div>
                    <div class="row">
                      <div  class="col-sm-9">
                        <sectionn>
                        <label class="select">
                          <select name="invoice-content">
                            <option value="0" selected disabled>选择流动人员</option>
                            <option value="1">张三三（证件号：2115781988010245687）</option>
                            <option value="2">张天天（证件号：2116781988010245687）</option>
                          </select>
                          <i></i> </label>
                        </section>
                      </div>
                      <div  class="col-sm-3">
                        <sectionn>
                        <label class="select">
                          <button type="submit" class="btn-u btn-block">添加</button>
                        </label>
                        </section>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
            <br/>
            <div class="row admin-box1">
              <div class="col-md-12 col-sm-12">
                <div class="panel panel-grey margin-bottom-10 table-responsive">
                  <div class="panel-heading">
                    <h3 class="panel-title"> 已添加人员</h3>
                  </div>
                  <table class="table table-hover sky-form">
                    <thead>
                      <tr>
                        <th>人员类型</th>
                        <th>名字</th>
                        <th>身份证号</th>
                        <th>开始时间</th>
                        <th>结束时间</th>
                        <th>查看</th>
                        <th>删除</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>其他人员</td>
                        <td>张三</td>
                        <td>1234561982120112345</td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                        <td><button class="btn btn-danger btn-xs">删除</button></td>
                      </tr>
                      <tr>
                        <td>流动人员</td>
                        <td>张四</td>
                        <td>1234561982120112345</td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                        <td><button class="btn btn-danger btn-xs">删除</button></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12 col-sm-12">
            <div class="headline">
              <h4>户主：李四</h4>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <form action="#" id="sky-form4" class="sky-form">
                  <fieldset style="padding:0px;">
                    <div class="row">
                      <div  class="col-sm-9">
                        <sectionn>
                        <label class="select">
                          <select name="invoice-content">
                            <option value="0" selected disabled>选择其他用户</option>
                            <option value="1">张三三（证件号：2115781988010245687）</option>
                            <option value="2">张天天（证件号：2116781988010245687）</option>
                          </select>
                          <i></i> </label>
                        </section>
                      </div>
                      <div  class="col-sm-3">
                        <sectionn>
                        <label class="select">
                          <button type="submit" class="btn-u btn-block">添加</button>
                        </label>
                        </section>
                      </div>
                    </div>
                    <div class="row">
                      <div  class="col-sm-9">
                        <sectionn>
                        <label class="select">
                          <select name="invoice-content">
                            <option value="0" selected disabled>选择流动人员</option>
                            <option value="1">张三三（证件号：2115781988010245687）</option>
                            <option value="2">张天天（证件号：2116781988010245687）</option>
                          </select>
                          <i></i> </label>
                        </section>
                      </div>
                      <div  class="col-sm-3">
                        <sectionn>
                        <label class="select">
                          <button type="submit" class="btn-u btn-block">添加</button>
                        </label>
                        </section>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
            <br/>
            <div class="row admin-box1">
              <div class="col-md-12 col-sm-12">
                <div class="panel panel-grey margin-bottom-10 table-responsive">
                  <div class="panel-heading">
                    <h3 class="panel-title"> 已添加人员</h3>
                  </div>
                  <table class="table table-hover sky-form">
                    <thead>
                      <tr>
                        <th>人员类型</th>
                        <th>名字</th>
                        <th>身份证号</th>
                        <th>开始时间</th>
                        <th>结束时间</th>
                        <th>查看</th>
                        <th>删除</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>其他人员</td>
                        <td>张三</td>
                        <td>1234561982120112345</td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                        <td><button class="btn btn-danger btn-xs">删除</button></td>
                      </tr>
                      <tr>
                        <td>流动人员</td>
                        <td>张四</td>
                        <td>1234561982120112345</td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><label class="input">
                            <input type="text" name="address" value="选择时间">
                          </label></td>
                        <td><a href="manager-order-details.html" class="btn btn-info btn-xs">查看</a></td>
                        <td><button class="btn btn-danger btn-xs">删除</button></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <form action="#" id="sky-form4" class="sky-form">
              <fieldset style="padding:0px;">
                <hr class="hr-sm">
                <section>
                  <label class="label">备注：</label>
                  <textarea class="form-control" rows="6"></textarea>
                </section>
                <section class=" color-red">
                  <label class="label"><b>错误提示信息</b></label>
                </section>
              </fieldset>
              <footer style="padding:0px;">
                <button type="submit" class="btn-u">保存</button>
              </footer>
            </form>
          </div>
        </div>
      </div>
    </div>
    
    <!--/end container--> 
    <!-- End Content Boxes --> 
    
  </div>
  <!--=== End Content Side Left Right ===--> 
</div>
<!--/wrapper--> 

<!-- JS Global Compulsory --> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery/jquery.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery/jquery-migrate.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins --> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/back-to-top.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/jquery-appear.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/plugins/smoothScroll.js"></script> 
<!-- JS Customization --> 
<!-- JS Page Level --> 
<script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/app.js"></script> 
<script type="text/javascript">
    jQuery(document).ready(function() {
      	App.init();
        App.initSidebarMenuDropdown();
       
    });
</script> 
<!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/assets/plugins/respond.js"></script>
    <script src="${pageContext.request.contextPath}/assets/plugins/html5shiv.js"></script>
    <script src="${pageContext.request.contextPath}/assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

</body>
</html>